#%%% Libraries

import json
import requests
import pandas as pd
import time
import numpy as np
from datetime import datetime
from dateutil import tz

#%%% collections and other files

with open('./parameters_list_bot.json', 'r') as f:
    params = json.load(f)

with open('./collections_list_bot.json', 'r') as f:
    collections = json.load(f)

addr = []
for col in collections:
    addr.append(col)

from_zone = tz.tzutc()
to_zone = tz.tzlocal()

#%%% Base Url and other parameters
os_base_url = "https://api.opensea.io/api/v1/events?only_opensea=true&asset_contract_address={}&event_type=created&occurred_after={}"
headers = {
    "Accept": "application/json",
    "X-API-KEY": params['bid_apiKey'][0]
}
epoch_time = int(time.time())
start_time = epoch_time - params['events_lookback']

#%%
assets_bid = {}
last_check = {}

while True:
    print('Checking events for txns.')
    for add in addr:
        if add not in last_check.keys():
            time_use = start_time
        else:
            time_use = last_check[add]
        epoch_time = int(time.time())
        last_check[add] = epoch_time
        os_url = os_base_url.format(add, time_use)
        add_list = requests.get(os_url, headers=headers)
        while add_list.status_code != 200:
            time.sleep(1.5)
            add_list = requests.get(os_url, headers=headers)
        all_events = add_list.json()['asset_events'].copy()
        next_page = 1
        while next_page == 1:
            if add_list.json()['next'] is None:
                next_page = -1
            else:
                cursor = add_list.json()['next']
                os_url_new = "https://api.opensea.io/api/v1/events?only_opensea=true&asset_contract_address={}&event_type=created&occurred_after={}&cursor={}".format(add, time_use,cursor)
                add_list = requests.get(os_url_new, headers=headers)
                while add_list.status_code != 200:
                    time.sleep(1.5)
                    add_list = requests.get(os_url, headers=headers)
                all_events.extend(add_list.json()['asset_events'])
        for events in all_events:
            if not events['asset'] is None:
                if not add + '_' + events['asset'][
                        'token_id'] in assets_bid.keys():
                    assets_bid[add + '_' + events['asset']['token_id']] = {}
                    assets_bid[add + '_' +
                               events['asset']['token_id']]['address'] = add
                    assets_bid[add + '_' + events['asset']['token_id']][
                        'token_id'] = events['asset']['token_id']
                    assets_bid[add + '_' + events['asset']['token_id']][
                        'token_name'] = events['asset']['collection']['slug']
                    d = datetime.strptime(events['listing_time'].split('.')[0],
                                          "%Y-%m-%dT%H:%M:%S")
                    epoch_time_list = (d -
                                       datetime(1970, 1, 1)).total_seconds()
                    assets_bid[add + '_' + events['asset']
                               ['token_id']]['list_time'] = epoch_time_list
                else:
                    d = datetime.strptime(events['listing_time'].split('.')[0],
                                          "%Y-%m-%dT%H:%M:%S")
                    epoch_time_list = (d -
                                       datetime(1970, 1, 1)).total_seconds()
                    if epoch_time_list >= assets_bid[
                            add + '_' +
                            events['asset']['token_id']]['list_time']:
                        assets_bid[add + '_' +
                                   events['asset']['token_id']] = {}
                        assets_bid[
                            add + '_' +
                            events['asset']['token_id']]['address'] = add
                        assets_bid[add + '_' + events['asset']['token_id']][
                            'token_id'] = events['asset']['token_id']
                        assets_bid[add + '_' + events['asset']
                                   ['token_id']]['token_name'] = events[
                                       'asset']['collection']['slug']
                        d = datetime.strptime(
                            events['listing_time'].split('.')[0],
                            "%Y-%m-%dT%H:%M:%S")
                        epoch_time_list = (
                            d - datetime(1970, 1, 1)).total_seconds()
                        assets_bid[add + '_' + events['asset']
                                   ['token_id']]['list_time'] = epoch_time_list

        time.sleep(1)
    items_del = []
    for items in assets_bid.keys():
        if (int(time.time()) - assets_bid[items]['list_time']) >= params['events_lookback']:
            items_del.append(items)
    [assets_bid.pop(key) for key in items_del]
    print(len(assets_bid.keys()))
    with open("items_to_bid_on.json", "w") as outfile:
        json.dump(assets_bid, outfile)
    print('File saved. Waiting 5 minutes.')
    time.sleep(300)
